var axios = require('axios');

var Cookies =require('js-cookie');
var host = (document.location.hostname==='jistapp.scalingo.io' || document.location.hostname==='jist.it' || document.location.hostname==='www.jist.it' )?'https://jist-api.scalingo.io/api/v1':'http://192.168.99.100:4000/api/v1';

if((document.location.hostname!='jistapp.scalingo.io' && document.location.hostname!='jist.it'  && document.location.hostname != 'www.jist.it')){
	Cookies.set('token','$$fake');
}

function getHttp(path,data){
	return axios.get(host+path,{params:data?data:{},headers: {'token': getToken()}})
}
function postHttp(path,data){
	return axios.post(host+path,data?data:{},{headers: {'token': getToken()}})
}

function getToken(){
	var t = Cookies.get('token');
	return t;
}

module.exports = {
	getHttp:function(path,data){
		return new Promise(function(resolve, reject) {
			getHttp(path, data).then(function (res) {
				if(res.data.error){
					reject(res.data.error);
				}else{
					resolve(res.data);
				}
			})
		});
	},
	postHttp:function(path,data){
		return new Promise(function(resolve, reject) {
			postHttp(path, data).then(function (res) {
				if(res.data.error){
					reject(res.data.error);
				}else{
					resolve(res.data);
				}
			})
		});
		//return postHttp(path,data).then(function(res){
		//	return res.data;
		//})
	}
}