var _ = require('lodash');
import React from 'react';


var coub_re = /^.*coub.com\/view/i;
var gif_re = '';
var link_re = '';
function checkUrlByType(type,url){
	if(!type){return '';}
	switch(type){
		case "gif":
			return true;
			break;
		case "link":
			return true;
			break;
		case "coub":
			return coub_re.test(url)
			break;
		default:
			console.error('unknown type',type);
			break;
	}
}

function getPreview(type, data){
	if(!type){return '';}
	switch(type){
		case "coub":
			return data.embed;
			break;
		case "link":
			return '<div style="background-image: url('+data.image+');     background-size: contain;background-repeat: no-repeat;background-position: 50%;width: 100%;height: 100%;"}}></div>';
		default:
			console.error('unknown type',type);
			break;
	}
}



module.exports = {
	checkUrlByType:checkUrlByType,
	getPreview:getPreview
}