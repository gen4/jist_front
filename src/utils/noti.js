var _ = require('lodash');
//import { OrderedSet } from 'immutable';

var notis = [];
var counter = 0;
var changeCallback;
function addNoti(data){
	data.key = counter++;
	data.dismissAfter = 3000;
	data.barStyle = (data.type=='danger')?{backgroundColor:'#e84135'}:{backgroundColor:'#97cd76'};
	console.log('>> ',data);
	notis.push(data);
	if(changeCallback){changeCallback()}

}
function deleteNoty(data){
	notis = _.reject(notis,{key:data.key})
	console.log('del ',notis.length);
	if(changeCallback){changeCallback()}
}



function setChange(callback){
	changeCallback = callback;
}

function getArray(){
	return _.clone(notis,true);
}

module.exports = {
	addNoti:addNoti,
	deleteNoty:deleteNoty,
	getArray:getArray,
	setChange:setChange
};

//import Noti from '../utils/noti';
//Noti.addNoti({
//	message: 'Notification message',
//	type:Math.random()>0.5?'danger':'other'
//})