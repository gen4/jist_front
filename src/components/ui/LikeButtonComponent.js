'use strict';

import React from 'react';

require('styles/ui/LikeButton.scss');

class LikeButtonComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      votersShown:false,
      myVote:this.props.myVote
    };
  }

  onLike(voteWieght){
    if(this.state.myVote == voteWieght){
      return;
    }else{
      if(!this.props.inactiveMode) {
        this.setState({myVote: voteWieght});
        this.hideVoters();
        this.props.onAction(voteWieght);
      }
    }


  }

  showVoters(){
    if(!this.props.inactiveMode){
      this.setState({
        votersShown:true
      });
    }

  }

  hideVoters(){
    this.setState({
      votersShown:false
    });
  }

  render() {

    var voters = (<div className="voters">
      <div className="multipliers">
        <div className={"down-vote vote-item  "+ ((this.state.myVote==-3)?'disabled':'clickable')} onClick={this.onLike.bind(this,-3)}>-3</div>
        <div className={"up-vote vote-item "+ ((this.state.myVote==3)?'disabled':'clickable')} onClick={this.onLike.bind(this,3)}>+3</div>
      </div>
      <div className={"down-vote vote-item "+ ((this.state.myVote==-1)?'disabled':'clickable')} onClick={this.onLike.bind(this,-1)}>-1</div>
      <div className={"up-vote vote-item "+ ((this.state.myVote==1)?'disabled':'clickable')} onClick={this.onLike.bind(this,1)}>+1</div>
    </div>);
    var display = (<div className="display vote-item is-info">{this.props.likes} likes</div>);
    var content = this.state.votersShown?voters:display;
    return (
      <div className="likebutton-component" onMouseEnter={this.showVoters.bind(this)} onMouseLeave={this.hideVoters.bind(this)}>
        {content}
      </div>
    );
  }
}

LikeButtonComponent.displayName = 'UiLikeButtonComponent';


// Uncomment properties you need
// LikeButtonComponent.propTypes = {};
// LikeButtonComponent.defaultProps = {};

export default LikeButtonComponent;
