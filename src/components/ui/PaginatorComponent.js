'use strict';

import React from 'react';

require('styles/ui/Paginator.scss');

class PaginatorComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage:0,
      maxButtons:5
    };
  }
//class="is-active"
  nextPage(){
    this.setState({currentPage:this.state.currentPage+1})
  }

  prevPage(){
    this.setState({currentPage:this.state.currentPage-1})
  }

  gotoPage(num){
    this.setState({currentPage:num})
  }

  render() {
    var that = this;

    var base = Math.round(that.state.currentPage - that.state.maxButtons/2);
    var maxBase= (that.props.pagesCount>that.state.maxButtons)?that.props.pagesCount-that.state.maxButtons:that.state.pagesCount;
    var initialBase = base;
    var middles = _.map(new Array(Math.min(this.state.maxButtons,this.props.pagesCount)),function(item,index){

      if(base<0){
        base=0;
      }
      if(base>maxBase){
        base=maxBase;
      }

      var num = base+index;

      var itemStyle = (num == that.state.currentPage)?'is-info':''
      return <li key={index} >
        <a className={itemStyle} onClick={that.gotoPage.bind(that,num)}>{num+1}</a>
      </li>
    });


    var com = <li>
                <span>...</span>
              </li>;

    var leftCom = (base>0)?com:'';
    var rightCom = (base<maxBase)?com:'';
    var firstPage =(base>0)?(<li>
      <a  onClick={that.gotoPage.bind(that,0)}>1</a>
    </li>):'';
    var lastPage = (base<maxBase)?(<li>
      <a  onClick={that.gotoPage.bind(that,that.props.pagesCount-1)}>{that.props.pagesCount}</a>
    </li>):'';

      var prevBut = (this.props.pagesCount>0 && base>0)?(<a onClick={that.prevPage.bind(that)}>Previous</a>):'';
      var nextBut = (this.props.pagesCount>0 && base<maxBase)?(<a onClick={that.nextPage.bind(that)}>Next page</a>):'';

    return (

        <nav className="pagination paginator-component">
            {prevBut}
            {nextBut}
          <ul>
            {firstPage}{leftCom}
            {middles}
            {rightCom}{lastPage}
          </ul>
        </nav>

    );
  }
}

PaginatorComponent.displayName = 'UiPaginatorComponent';

// Uncomment properties you need
// PaginatorComponent.propTypes = {};
// PaginatorComponent.defaultProps = {};

export default PaginatorComponent;
