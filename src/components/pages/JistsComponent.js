'use strict';

import React from 'react';
import API from '../../utils/api';
import { Link } from 'react-router';
import {browserHistory} from 'react-router';

require('styles/pages/Jists.scss');

class JistsComponent extends React.Component {
  constructor(props) {
    super(props);
    var that = this;
    this.state = {jistsList: props.jistsList};
    API.getHttp('/jists').then(function(data){
      console.log('AOOPOI ',data);
      that.setState({
        jistsList:data
      });
    })
  }

  goToJist(item,e){
    console.log(item,":",e);
    browserHistory.push('/jists/'+item._id);
    //this.router.transitionTo('/jist/'+item._id);
  }
  render() {
    var jists = this.state.jistsList.map(function(item,index){
      return (<tr onClick={this.goToJist.bind(this,item)} key={index}>

        <td>{index}</td>
        <td>{item.title}</td>

      </tr>)
    }.bind(this));

    return (
      <table className="table is-striped">
        <tbody>
        {jists}
        </tbody>
      </table>

    );
  }
}
JistsComponent.propTypes = { jistsList: React.PropTypes.array };
JistsComponent.defaultProps = { jistsList: [] };
JistsComponent.displayName = 'PagesJistsComponent';

// Uncomment properties you need
// JistsComponent.propTypes = {};
// JistsComponent.defaultProps = {};

export default JistsComponent;
