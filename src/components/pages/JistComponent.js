'use strict';

import React from 'react';
import API from '../../utils/api';
import Jistlist from '../JistlistComponent';
import Modal from 'react-modal';
import Update from 'react-addons-update';
import Noti from '../../utils/noti';
import TUtils from '../../utils/typesUtils';
import Cookies from 'js-cookie';

var classNames = require('classnames');

require('styles/pages/Jist.scss');


class JistComponent extends React.Component {
  constructor(props) {
    super(props);
    console.log('cnstr');

    this.state = {
      jist: {},
      addNewItemModal:{
        url:'',
        isLoading:false,
        data:{
          title:'',
          desc:'',
          embded:''
        }
      },
      urlIsValid:false,
      modalIsOpen:false,
      isAdditionalFieldsShown:false
    };
    this.loadJist();
  }

  loadJist(){
    var that = this;
    API.getHttp('/jists/'+this.props.params.jid).then(function(data){
      that.setState({
        jist:data
      });
    })
  }

  openAddNewItem(){
    this.setState({modalIsOpen: true});
  }

  closeAddNewItem(){
    this.clearAddNewItemModal();
    this.setState({modalIsOpen: false});
  }

  clearAddNewItemModal(){
    this.setState({
      addNewItemModal:{
        url:'',
        isLoading:false,
        data:{
          title:null,
          desc:'',
          embded:null
        }
      },
      urlIsValid:false,
      isAdditionalFieldsShown:false
    });
  }

  checkUrl(){
    var that = this;
    //TODO TypeDependece Check
    if(this.state.isAdditionalFieldsShown){
      this.clearAddNewItemModal()
    }else{
      var newData = Update(this.state,{addNewItemModal:{isLoading:{$set:true}}});
      this.setState(newData);


      API.postHttp('/itemTypes/check',{data:{url:this.state.addNewItemModal.url},jid:this.props.params.jid}).then(function(data){
        console.log('CHECK RE',data);
        var newData = Update(that.state,{
          isAdditionalFieldsShown:{$set:true},
          addNewItemModal:{
            isLoading:{$set:false},
            data:{$set:data}
          }
        });
        that.setState(newData);
      },function(error){
        console.error(error);
        Noti.addNoti({
          message: 'Error'+(error.message?(":"+error.message):''),
          type:'danger'
        });
      })
    }
  }

  urlChange(event){
    console.log('U C ')

    var newData = Update(this.state,{
      addNewItemModal:{
        url:{$set:event.target.value},
        isLoading:{$set:false}
      },
      urlIsValid: {$set: (event.target.value.length>0 && TUtils.checkUrlByType(this.state.jist.type,event.target.value))}});
    this.setState(newData);
  }

  titleChange(event) {
    console.log('.',event.target.value);
    var newData = Update(this.state,{
      addNewItemModal: {
        data:{
          title: {$set:event.target.value}
        }
      }
    });
    this.setState(newData);
  }

  descChange(event) {
    var newData = Update(this.state,{
      addNewItemModal: {
        data:{
          desc: {$set:event.target.value}
        }
      }
    });
    this.setState(newData);
  }

  addNewItem(){
    var obj = {
      title:this.state.addNewItemModal.data.title,
      desc:this.state.addNewItemModal.data.desc,
      url:this.state.addNewItemModal.url
    };
    var that = this;
    API.postHttp('/jists/'+this.props.params.jid+'/items',obj).then(function(data){
        console.log('add complete',data);
      that.closeAddNewItem();
      that.refs['jistlist'].getList();
      Noti.addNoti({
        message: 'Item added',
        type:'info'
      })
    },function(error){
      console.log('err',error);
      Noti.addNoti({
        message: 'Error'+(error.message?(":"+error.message):''),
        type:'danger'
      });
    });
  }

  render() {
    var inputUrlClasses = classNames({
      'input':true,
      'is-info':true,
      'is-disabled':this.state.isAdditionalFieldsShown
    });

    var checkWord = this.state.isAdditionalFieldsShown?(<i className="fa fa-times"></i>):'Check';

    var checkButtonClasses = classNames({
      'is-disabled': ( !this.state.urlIsValid ),
      'is-loading': ( this.state.addNewItemModal.isLoading ),
      'button':true,
      'is-info':true
    });

    var addInfoClasses = classNames({
      'is-hidden':( this.state.isAdditionalFieldsShown == false ),
    });

    var customStyles = {
      content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        padding               : '0px',
        width                 : this.state.isAdditionalFieldsShown?'50%':'30%',
        transform             : 'translate(-50%, -50%)'
      }
    };

    console.log('> EE ',this.state.jist)
    var preview = TUtils.getPreview(this.state.jist.type, this.state.addNewItemModal.data);
    var modalTypeDependedContent = (
        <div className="control columns">

          <div className="column is-6">
            <div className="coub-wrapper adding">
              <div style={{height:'100%'}} dangerouslySetInnerHTML={{__html: preview}}></div>
            </div>
          </div>

          <div className="column is-6">
            <p className="control">
              <label className="label">Title</label>
              <input className="input is-info" type="text" maxLength="140" placeholder="Title" onChange={this.titleChange.bind(this)} value={this.state.addNewItemModal.data.title}/>
            </p>
            <p className="control">
              <label className="label">Description</label>
              <textarea className="textarea addnewcoub-textarea" maxLength="400" placeholder="Description" rows="3" onChange={this.descChange.bind(this)} value={this.state.addNewItemModal.data.desc}></textarea>
            </p>
          </div>

        </div>
    );

    var modal = (
        <Modal
        isOpen={this.state.modalIsOpen}
        onRequestClose={this.closeAddNewItem.bind(this)}
        style={customStyles}
          >

            <div className="content">
              <div className=" hero is-info">
                <div className="padding ">
                  <h1 className="modal-header">Add new {this.state.jist.type}</h1>
                </div>
              </div>

              <div className="padding">
                <p className="control is-grouped">
                  <input className={inputUrlClasses} type="text" placeholder="Paste URL here" onChange={this.urlChange.bind(this)} value={this.state.addNewItemModal.url}/>
                  <button className={checkButtonClasses} onClick={this.checkUrl.bind(this)}>{checkWord}</button>
                </p>


                <div className={addInfoClasses}>
                  {modalTypeDependedContent}
                  <div className="control columns">
                    <div className="column">
                      <button className="button is-info is-fullwidth" onClick={this.addNewItem.bind(this)}>ADD</button>
                    </div>
                  </div>
                </div>

              </div>
            </div>
        </Modal>
        );

var addNewItemButton = Cookies.get('token')?(<button className="button is-info margin-left" onClick={this.openAddNewItem.bind(this)}>
                          <i className="fa fa-plus"> </i> <span>Add new item</span>
                        </button>):(<button className="button is-info margin-left is-disabled">
                            <i className="fa fa-plus"> </i> <span>Add new item</span>
                          </button>);
    return (

        <div className=" container margin-top  ">
          {modal}
          <div className="section padding-side">
            <div className="columns">
              <div className="column is-8">
               <span className="title is-3  create-header">{this.state.jist.title}</span>
              </div>
              <div className="column is-4 is-text-right">
                {addNewItemButton}

              </div>
            </div>
            <Jistlist jid={this.props.params.jid} ref="jistlist"></Jistlist>
          </div>
        </div>
    );
  }
}

JistComponent.displayName = 'PagesJistComponent';

//<button className="button ">
//  <i className="fa fa-ellipsis-h"></i>
//</button>
// Uncomment properties you need
// JistComponent.propTypes = {};
// JistComponent.defaultProps = {};

export default JistComponent;
