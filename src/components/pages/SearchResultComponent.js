'use strict';

import React from 'react';
import API from '../../utils/api';
import { Link, browserHistory } from 'react-router';
import Paginator from '../ui/PaginatorComponent';

require('styles/pages/SearchResult.scss');

class SearchResultComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jists: [],
      page:0,
      pagesCount:1,
      query:'',
      newQuery:{
        s:this.props.location.query.s
      }
    };
    this.loadJist(this.props.params,0);

  }

  componentWillReceiveProps(a,b){
    console.log('componentWillReceiveProps',a,b);
    if(a.params.mode=='r'){
      this.setState({
        query:''
      })
    }
    this.loadJist(a.params,0);
  }


  search(){
    if(this.state.query.length>0){
      browserHistory.push('/search/s/'+this.state.query);
    }
  }


  loadJist(params, currentPage){

    var that = this;

    var mode = params.mode ;
    var q = params.query;
    if(!mode || !q){
      mode = 'r';
      q = 'best';
    }
    var obj = {};
    obj[mode] = q;
    var request = _.assign(obj,{page:currentPage || 0});
    console.log('load',request);
    //var query = _.reduce(request,function(total,value,key){
    //  return total+key+"="+value+'&'
    //},'');
    API.getHttp('/jists',request).then(function(data){
      console.log('answer',data);
        that.setState({
            jists:data.items,
            pagesCount:data.pages
        });
    })
  }

  onChange(){
    this.setState({
      query:event.target.value
    })
  }

  goToJist(item,e){
    browserHistory.push('/jists/'+item._id);
  }
  onKeyPress(e){
    if(e.charCode==13){
      this.search()
    }
  }

  render() {
    console.log('render')
    var jists = this.state.jists.map(function(item,index){
      return (<tr className="clickable card " onClick={this.goToJist.bind(this,item)} key={index}>

        <td className="super-padding">
          <p className="title is-3 results-title">{item.title}</p>
          <p className="subtitle is-6 results-desc">{item.desc}</p>

        </td>
        <td className="is-text-centered super-padding">
          <p className="title is-3">{item.counters.answers}</p>
          <p className="subtitle is-6">{item.type}s</p>
        </td>



      </tr>)
    }.bind(this));
    return (

        <div className="search-fullheight container">
            <div className="containter padding-side">
              <input className="input is-info result-search-input" type="text" value={this.state.query}  onKeyPress={this.onKeyPress.bind(this)} onChange={this.onChange.bind(this)} placeholder="What are you looking for?"/>

              <table className="table is-striped">
                <tbody>
                {jists}
                </tbody>
              </table>

              <Paginator onChange={this.loadJist.bind(this)} pagesCount={this.state.pagesCount} ></Paginator>

            </div>
        </div>
    );
  }
}

SearchResultComponent.displayName = 'PagesSearchResultComponent';

// Uncomment properties you need
// SearchResultComponent.propTypes = {};
// SearchResultComponent.defaultProps = {};

export default SearchResultComponent;
