'use strict';

import React from 'react';
import API from '../../utils/api';
import { browserHistory } from 'react-router'
import Noti from '../../utils/noti';


require('styles/pages/CreateJist.scss');

class CreateJistComponent extends React.Component {
  constructor(props) {
    super(props);
    var that = this;
    this.state = {
        itemTypes: [],
        newListParams:{
            title:'',
            desc:'',
            type:''
        }
    };
    API.getHttp('/itemTypes').then(function(data){
      that.setState({
        itemTypes:data,
          newListParams:{type:data[0]}
      });
    })
  }
    handleChange(field){
        return function(event){
            var obj = Object.assign(this.state,{})
            obj.newListParams[field] =  event.target.value;
            this.setState(obj);
        }
    }
  submit(){
    //console.log('ON SLET',this.state.newListParams);
    API.postHttp('/jists',this.state.newListParams).then(function(res){
        console.log('dd',res)
        browserHistory.push('/jists/'+res._id);
        Noti.addNoti({
            message: 'Jist created',
            type:'info'
        })

    },function(error){
        Noti.addNoti({
            message: 'Error'+(error.message?(":"+error.message):''),
            type:'danger'
        });
    })
  }

  render() {
    return (
      <div className=" container margin-top  ">
      <div className="section padding-side">
        <div className="columns">
          <span className="title is-3 create-header">Create new jist of </span>
            <span className="select is-large-select is-info">
              <select className="select is-info" onChange={this.handleChange('type').bind(this)}>
                {this.state.itemTypes.map(function(item,index){
                  return (<option className="is-info" value={item} key={index}>{item}s</option>)
                })}
              </select>
            </span>
        </div>
        <div className="columns">
          <p className="column control">
            <input className="input is-large is-info" type="text" maxLength="140" placeholder="Title" onChange={this.handleChange('title').bind(this)}/>
          </p>
        </div>
        <div className="columns">
          <div className="column ">
            <textarea className="textarea is-large is-info" maxLength="400" rows="4" cols="50" placeholder="Description" onChange={this.handleChange('desc').bind(this)}>

            </textarea>
          </div>
        </div>
        <p className="control">
          <button className="button is-large is-info" onClick={this.submit.bind(this)}>Submit</button>
        </p>
          </div>
      </div>

    );
  }
}

CreateJistComponent.displayName = 'PagesCreateJistComponent';

// Uncomment properties you need
 CreateJistComponent.propTypes = {};
 CreateJistComponent.defaultProps = {};

export default CreateJistComponent;
