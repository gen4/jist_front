'use strict';

import React from 'react';
import { Link, browserHistory } from 'react-router'


require('styles/pages/Search.scss');

class SearchComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {query: ''};
    }

    gotoBest(){
        browserHistory.push('/search/r/best');
    }

    search(){
        if(this.state.query.length>0){
            browserHistory.push('/search/s/'+this.state.query);
        }
    }

    onKeyPress(e){
        if(e.charCode==13){
           this.search()
        }
    }

    onChange(event){
        this.setState({query:event.target.value})
    }

  render() {
    return (
        <div className="search-fullheight container">
            <div className="hero-content padding-side">
                <button className="button" onClick={this.gotoBest.bind(this)}> Best jists</button>
            </div>
          <div className="hero-content search-table">
            <div className=" search-table-cell">
                <div className="containter padding-side">
                        <input className="input is-large is-info main-searchbar" onKeyPress={this.onKeyPress.bind(this)} onChange={this.onChange.bind(this)} type="text" placeholder="What are you looking for?"/>
                </div>
                <small className="padding-side">ex: Weird movies</small>
            </div>
          </div>
        </div>
    );
  }
}

SearchComponent.displayName = 'PagesSearchComponent';
//<Link to="/results">
//    <button className="button is-info">all Jists</button>
//</Link>
// Uncomment properties you need
// SearchComponent.propTypes = {};
// SearchComponent.defaultProps = {};

export default SearchComponent;
