'use strict';

import React from 'react';

require('styles/pages/Profile.scss');

class ProfileComponent extends React.Component {
  render() {
    return (
      <div className="profile-component">
        Please edit src/components/pages//ProfileComponent.js to update this component!{this.props.params.id}
      </div>
    );
  }
}

ProfileComponent.displayName = 'PagesProfileComponent';

// Uncomment properties you need
// ProfileComponent.propTypes = {};
// ProfileComponent.defaultProps = {};

export default ProfileComponent;
