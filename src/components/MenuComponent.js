'use strict';

import React from 'react';
import { Link } from 'react-router';
import Modal from 'react-modal';
import API from '../utils/api';
import Cookies from 'js-cookie';

require('styles//Menu.scss');

class MenuComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen:false,
            user:Cookies.get('token'),
            loginData:{
                email:'',
                password:''
            }
        };
    }

    openLoginModal(){
        this.setState({modalIsOpen: true});
    }

    closeLoginModal(){
        //this.clearAddNewItemModal();
        this.setState({modalIsOpen: false});
    }

    emailChange(event){
        var newData = Update(this.state,{
            loginData:{
                email:{$set:event.target.value},
            }
        });
        this.setState(newData);
    }
    passwordChange(event){
        var newData = Update(this.state,{
            loginData:{
                login:{$set:event.target.value},
            }
        });
        this.setState(newData);
    }
    //onRequestClose={this.closeAddNewItem.bind(this)}


    login(){
        var that = this;
        API.postHttp('/auth/login/',this.state.loginData).then(function(data){
            console.log('login',data)
            //that.setState({
            //    jist:data
            //});
        })
    }

    gotoFacebook(){
        window.location.href ="/auth/facebook"
    }

    gotoVkontakte(){
        window.location.href ="/auth/vkontakte"
    }

    logout(){
        console.log('logout')
        Cookies.remove('token');
        this.setState({
            user:null
        });
        location.reload();
    }




  render() {
      var customStyles = {
          content : {
              top                   : '50%',
              left                  : '50%',
              right                 : 'auto',
              bottom                : 'auto',
              marginRight           : '-50%',
              padding               : '0px',
              width                 : '30%',
              transform             : 'translate(-50%, -50%)'
          }
      };
      var modal = (
          <Modal
              isOpen={this.state.modalIsOpen}
              style={customStyles}
              onRequestClose={this.closeLoginModal.bind(this)}
          >

              <div className="content">
                  <div className=" hero is-info">
                      <div className="padding ">
                          <h1 className="modal-header">Login</h1>
                      </div>
                  </div>

                  <div className="padding">
                          <div className="divider"></div>
                          <p className="control">
                                  <button className="button is-info is-block  is-fullwidth is-large" onClick={this.gotoFacebook.bind(this)}>
                                      FB
                                  </button>
                          </p>
                          <p className="control">
                              <button className="button is-info is-block  is-fullwidth is-large" onClick={this.gotoVkontakte.bind(this)}>
                                  VK
                              </button>
                          </p>
                  </div>
              </div>
          </Modal>
      );
      var loginButton = (<div className="white-text clickable" onClick={this.openLoginModal.bind(this)}>Login</div>);
      var logoutButton = (<div className="white-text clickable" onClick={this.logout.bind(this)}>Logout</div>);
      var logInOutButton = this.state.user?logoutButton:loginButton;

      var createNewButton = this.state.user?(<Link to="/create" ><div className="button is-success">Create new jist</div></Link>):(<div className="button is-success is-disabled" onClick={this.openLoginModal.bind(this)}>Create new jist</div>)

    return (
        <div className="hero is-info  ">
            {modal}
        <div className="navbar container padding" >
            <div className="navbar-left">
                <Link className="navbar-item logo" to="/">
                    <span className="title  main">Jist</span> <span className="tag sub">v.0.1 beta </span>
                </Link>
            </div>
            <div className="navbar-right ">
              <span className="navbar-item">
                  {createNewButton}
              </span>
              <span className="navbar-item">
                  {logInOutButton}
              </span>
            </div>
        </div>
        </div>
    );
  }
}

MenuComponent.displayName = 'MenuComponent';

// Uncomment properties you need
// MenuComponent.propTypes = {};
// MenuComponent.defaultProps = {};

//<p className="control">
//    <Link className="" to="/auth/vkontakte">
//        <button className="button is-info is-block  is-fullwidth is-large">
//            VK
//        </button>
//    </Link>
//</p>



//<p className="control has-icon is-large">
//    <input className="input is-large" type="email" placeholder="Email" onChange={this.emailChange.bind(this)} value={this.state.loginData.email}/>
//    <i className="fa fa-envelope"></i>
//</p>
//<p className="control has-icon is-large ">
//    <input className="input is-large" type="password" placeholder="Password" onChange={this.passwordChange.bind(this)} value={this.state.loginData.password}/>
//<i className="fa fa-lock "></i>
//    </p>
//    <p className="control">
//    <button className="button is-success is-block is-fullwidth" onClick={this.login.bind(this)}>
//Login
//</button>
//</p>

export default MenuComponent;
