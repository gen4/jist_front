'use strict';

import React from 'react';
//import ItemGif from './GifComponent'
import ItemCoub from './CoubComponent'
import ItemLink from './LinkComponent'
//import ItemNote from './NoteComponent'
import LikeButton from '../ui/LikeButtonComponent'
import API from '../../utils/api';
import Cookies from 'js-cookie';
import Moment from 'moment';

require('styles/item/Common.scss');
//
class CommonComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            likes:this.props.item.totalVotes
        };
    }

    getMainContent(){
        switch (this.props.item.type){
            case 'coub':
                return (<ItemCoub item={this.props.item}></ItemCoub>);
                break;
            case 'link':
                return (<ItemLink item={this.props.item}></ItemLink>);
                break;
            default:
                break;
        }
    }

    onLike(voteWeight){
        console.log('LIKE',voteWeight);
        this.setState({
            likesLoading:true
        });
        var that = this;
        API.postHttp('/items/'+this.props.item._id+'/votes',{vote:voteWeight}).then(function(data){
            console.log('answer',data);
            that.setState({
                likes:data.totalVotes
            });
        })

    }





  render() {

    var mainContent = this.getMainContent();
    var inactiveLike = !Cookies.get('token');
    var datetime  = Moment(this.props.item.createdAt).fromNow();

    return (
      <div className="margin-top">
        <div className="card is-fullwidth-strong">
          <div className="card-content no-padding">
            <div className="card-custom-header">
              <div className="card-custom-header-num ">
                <h3 className="title is-3 ">{this.props.index+1}</h3>


              </div>
              <div className="card-custom-header-left">
                <h3 className="title is-3 ">{this.props.item.title} </h3>
              </div>

              <div className="card-custom-header-right">
  <span className="tag">
                Fake tag
              </span>

              </div>
            </div>
          </div>

          <div className="card-image">
              {mainContent}
          </div>
          <div className="card-content is-text-right card-custom-footer">
            <div className="card-custom-footer-left">

              <small>{datetime}</small>
            </div>
            <div className="card-custom-footer-right">
                <LikeButton myVote={this.props.item.myVote} inactiveMode={inactiveLike} loading={this.state.likesLoading} likes={this.state.likes} onAction={this.onLike.bind(this)}></LikeButton>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

CommonComponent.displayName = 'ItemCommonComponent';


export default CommonComponent;
