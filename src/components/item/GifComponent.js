'use strict';

import React from 'react';

require('styles/item/Gif.scss');

class GifComponent extends React.Component {
  render() {
    return (
      <div className="gif-component">
        Please edit src/components/item//GifComponent.js to update this component!
      </div>
    );
  }
}

GifComponent.displayName = 'ItemGifComponent';

// Uncomment properties you need
// GifComponent.propTypes = {};
// GifComponent.defaultProps = {};

export default GifComponent;
