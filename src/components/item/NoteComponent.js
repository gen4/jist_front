'use strict';

import React from 'react';

require('styles/item/Note.scss');

class NoteComponent extends React.Component {
  render() {
    return (
      <div className="note-component">
        Please edit src/components/item//NoteComponent.js to update this component!
      </div>
    );
  }
}

NoteComponent.displayName = 'ItemNoteComponent';

// Uncomment properties you need
// NoteComponent.propTypes = {};
// NoteComponent.defaultProps = {};

export default NoteComponent;
