'use strict';

import React from 'react';

require('styles/item/Link.scss');

class LinkComponent extends React.Component {
  render() {
    var demstyle = {};
    if(+this.props.item.data.height<+this.props.item.data.width){
      demstyle = {
        paddingBottom:(Math.round(this.props.item.data.height/this.props.item.data.width*10000)/100)+'%'
      };
    }else{
      demstyle = {
        paddingBottom:'50%'
      };
    }

    var imgStyle={
      backgroundImage: 'url('+this.props.item.data.image+')',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
    };

    return (
        <div className="">
          <div className="coub-wrapper clickable" >
            <div className="" >
              <div className="link-desc-wrapper">
                <div className="link-img" style={imgStyle}></div>
                <div className="link-desc">
                  {this.props.item.desc}
                  <div>
                    <small>{this.props.item.data.url}</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

LinkComponent.displayName = 'ItemLinkComponent';

// Uncomment properties you need
// LinkComponent.propTypes = {};
// LinkComponent.defaultProps = {};

export default LinkComponent;
