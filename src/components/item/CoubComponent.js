'use strict';

import React from 'react';

require('styles/item/Coub.scss');

class CoubComponent extends React.Component {
  render() {

    var demstyle = {};
    if(+this.props.item.data.height<+this.props.item.data.width){
      demstyle = {
        paddingBottom:(Math.round(this.props.item.data.height/this.props.item.data.width*10000)/100)+'%'
      };
    }else{
      demstyle = {
        paddingBottom:'50%'
      };
    }
    return (
      <div className="">
        <div className="coub-wrapper" style={demstyle}>
          <iframe src={'//coub.com/embed/'+this.props.item.ext_id+'?muted=false&autostart=false&originalSize=false&startWithHD=false'} allowfullscreen="true" frameborder="0" ></iframe>
        </div>
      </div>
    );
  }
}

CoubComponent.displayName = 'ItemCoubComponent';
//width="640" height="384"
// Uncomment properties you need
// CoubComponent.propTypes = {};
// CoubComponent.defaultProps = {};

export default CoubComponent;
