require('normalize.css');
require('styles/App.css');
require('styles//Main.sass');

import React from 'react';
import Menu from './MenuComponent';
import { NotificationStack } from 'react-notification';
import Noti from '../utils/noti';
//import { OrderedSet } from 'immutable';

let yeomanImage = require('../images/yeoman.png');

class AppComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            notifications: Noti.getArray()
        };
        var that = this;
        Noti.setChange(function(){
            console.log('ipd')
            that.setState({
                notifications: Noti.getArray()
            });
        })
    }

  render() {
    return (
        <section className="is-fullheight">
            <NotificationStack
                className="noti-stack"
                notifications={this.state.notifications}
                onDismiss={notification => {
                    Noti.deleteNoty(notification);
                    this.setState({
                        notifications: Noti.getArray()
                    })}
                  }
            />
            <Menu className=""></Menu>
            <div className="is-fullheight">
                {this.props.children}
            </div>
        </section>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
