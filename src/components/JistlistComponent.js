'use strict';

import React from 'react';
import API from '../utils/api';
import CommonItem from './item/CommonComponent'
import Paginator from './ui/PaginatorComponent';

require('styles//Jistlist.scss');

class JistlistComponent extends React.Component {
  constructor(props) {
    super(props);
    var that = this;
    this.state = {
      items: [],
      pagesCount:12,
      currentPage:3
    };

  }

  getList(currentPage){
    console.log('get list! ');
    var that = this;
    API.getHttp('/jists/'+this.props.jid+'/items',{page:currentPage}).then(function(data){
      console.log(' > >> ',data);
      that.setState({
        items:data.items,
        pagesCount:data.pages
      });
      //that.render();
    })
  }

  componentWillMount(){
    this.getList();
  }

  render() {
    console.log('R ',this.state.pagesCount)
    var that = this;
    var items = this.state.items.map(function(item,index){
      return (
      <CommonItem jid={that.props.jid} key={index} index={index} item={item}></CommonItem>
      )
    });
    return (
      <div className="">
        <div>{items}</div>
        <Paginator onChange={this.getList.bind(this)} pagesCount={this.state.pagesCount} ></Paginator>
      </div>
    );
  }
}

JistlistComponent.displayName = 'JistlistComponent';

// Uncomment properties you need
// JistlistComponent.propTypes = {};
// JistlistComponent.defaultProps = {};

export default JistlistComponent;
