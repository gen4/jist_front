import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import Routes from "./routes";


// Render the main component into the dom
ReactDOM.render((<Router history={browserHistory}>{Routes}</Router>), document.getElementById('app'));

//export default AppComponent;