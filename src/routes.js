import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory, Redirect, IndexRedirect } from 'react-router'
import Main from './components/Main';
import Search from './components/pages/SearchComponent';
import Profile from './components/pages/ProfileComponent';
import Jists from './components/pages/JistsComponent';
import Jist from './components/pages/JistComponent';
import CreateJist from './components/pages/CreateJistComponent';
import SearchResult from './components/pages/SearchResultComponent';
var ga = require('react-ga');
ga.initialize('UA-76155029-1');

//ga.pageview('/');
function logPageView() {
	//console.log('log send',this.state.location.pathname);
	//ga.pageview(this.state.location.pathname);
}
browserHistory.listen(location => {
	console.log('log send',location.pathname);
	ga.pageview(location.pathname);
});


//
//class AppComponent extends React.Component {
//	render() {
//		return (
//		<Route path="/" component="{Main}"></Route>
//		);
//	}
//}
//
//AppComponent.defaultProps = {
//};
//
//export default AppComponent;
module.exports = (

	<Route path="/" component={Main} onUpdate={logPageView}  >


		<IndexRedirect to="/search/r/best" />
		<Route path="profile" onUpdate={logPageView}  component={Profile} />
		<Route path="create" onUpdate={logPageView}  component={CreateJist} />
		<Route path="results" onUpdate={logPageView}  component={Jists} />
		<Route path="search/:mode/:query" onUpdate={logPageView}  component={SearchResult} />
		<Route path="profile/:id" onUpdate={logPageView}  component={Profile} />
		<Route path="jists/:jid" onUpdate={logPageView}  component={Jist} />
	</Route>

);

//<IndexRoute component={Search} onUpdate={logPageView} />
//<IndexRedirect to="/search?r=best" />