const log					= require('./logs.js')(module);

const express 				= require('express');
const app 					= express();

//const db					= require('./db/db.js');
const authRoute		   	 	= require('./auth.route.js');
const path 					= require('path');
const session 				= require('express-session');
const cors 					= require('cors');
var bodyParser 				= require('body-parser');
var cookieParser 			= require('cookie-parser')
var passport				= require('passport');

app.use(cookieParser());
app.use(bodyParser.json({ type: 'application/json'}));
//app.use(session({ secret: 'awesome cosmos' ,cookie: { httpOnly: false, key:'token' },resave:false,saveUninitialized:false}));
app.use(passport.initialize());
//app.use(passport.session());
app.use('/auth', authRoute);
app.use(cors({ origin: ['http://localhost:8000','http://localhost:*'],credentials:true}));

app.use('/', express.static(path.resolve(__dirname,'../dist')));
app.use('/assets',express.static(path.resolve(__dirname,'../server/assets')))
app.get('*', function(req, res){
	res.sendFile(path.resolve(__dirname, '../dist/index.html'));
});


//db.init(function(err){
//	if(err){
//		log.error(err)
//	}else{
//
//	}
//});
startServer()
function startServer(){
	var port = process.env.PORT || 3000;
	app.listen(port, function () {
		log.info('Example app listening on port [%s]!',port);
	});
}
