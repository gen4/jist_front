const log					= require('./logs.js')(module);

const express				= require('express');
const router 				= express.Router();
const async					= require('async');
const request				= require('request');
const CONFIG				= require('./config.js');



var passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const VKontakteStrategy = require('passport-vkontakte').Strategy;

passport.serializeUser(function(user, done) {
	done(null, user.tokens[0]);
});

passport.deserializeUser(function(token, done) {
	if(token){
		done(null, {token:token});
	}else{
		done(null, null);
	}

});

passport.use(new FacebookStrategy({
		clientID: 206940959674703,
		clientSecret: "c029affc1f5a586a653ea81082b34701",
		callbackURL: CONFIG.PUBLIC_ADDRESS+"/auth/facebook/callback",
		enableProof: true
	},
	function(accessToken, refreshToken, profile, done) {
		request({
			uri:CONFIG.API_URL+'/users',
			method:'POST',
			body:{profile:profile},
			json:true
		}, function optionalCallback(err, httpResponse, body) {
			if (err) {
				 console.error('upload failed:', err);
				return done(err)
			}else{
				done(null,body);
			}
		});
	}
));

passport.use(new VKontakteStrategy({
		clientID:     5394883, // VK.com docs call it 'API ID'
		clientSecret: "3t2VsWgJJKUATqvn1fyB",
		callbackURL:  CONFIG.PUBLIC_ADDRESS+"/auth/vkontakte/callback",
		enableProof: true
	},
	function(accessToken, refreshToken, profile, done) {
			request({
				uri:CONFIG.API_URL+'/users',
				method:'POST',
				body:{profile:profile},
				json:true
			}, function optionalCallback(err, httpResponse, body) {
				if (err) {
					console.error('upload failed:', err);
					return done(err)
				}else{
					done(null,body);
				}
			});
	}
));

//passport.use(new LocalStrategy(
//	function(username, password, done) {
//		User.findOne({ registrations: User.getRegFromProfile({provider:'email',id:username})+":"+User.getHash(password) }, function (err, user) {
//			if (err) { return done(err); }
//			if (!user) {
//				return done(null, false, { message: 'Incorrect username or password' });
//			}
//			return done(null, user);
//		});
//	}
//));






router.get('/facebook',
	passport.authenticate('facebook'));
router.get('/facebook/callback',
	passport.authenticate('facebook', {failureRedirect: '/' }),successCallback);


router.get('/vkontakte',
	passport.authenticate('vkontakte'));
router.get('/vkontakte/callback',
	passport.authenticate('vkontakte', { failureRedirect: '/' }),successCallback);


//router.post('/login',
//	passport.authenticate('local', { successRedirect: '/',
//		failureRedirect: '/login' }));


function successCallback(req,res){
	if(req && req.user.tokens && req.user.tokens[0]){
		res.cookie('token',req.user.tokens[0])
	}
	res.redirect('/');
}

module.exports = router;