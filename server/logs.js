const _						= require('lodash');
const log4js 				= require('log4js');

module.exports = function(mod){
		return mod?log4js.getLogger(_.takeRight(mod.id.split('/'),2).join('/')):'root';
};