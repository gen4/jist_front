var _			= require('lodash');


var config = {
	PUBLIC_ADDRESS:'http://www.jist.it',
	API_URL:'https://jist-api.scalingo.io/api/v1'
}
var config_dev = {
	PUBLIC_ADDRESS:'http://local.foo.com:3000',
	API_URL:'http://192.168.99.100:4000/api/v1'
}



if(process.env.NODE_ENV!='production'){
	config = _.assign(config,config_dev);
}
console.log(config);
module.exports = config;